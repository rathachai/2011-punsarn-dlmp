/*---------------------------------------------*/
/*                   MODEL                     */
/*---------------------------------------------*/

var app = app || {};

var BookList = Backbone.Collection.extend({
	model: app.BookItem, 
	url: '/dlmp/site/json/books.json'
});

app.Books = new BookList();
