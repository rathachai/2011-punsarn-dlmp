/*---------------------------------------------*/
/*                   VIEW                      */
/*---------------------------------------------*/

var app = app || {};

app.AppView = Backbone.View.extend({
	el: '#appview',

	initialize: function() {
		this.$main = this.$('#main');
		this.listenTo(app.Books, 'all', this.render);

		app.Books.fetch();
	},
	render: function() {
		$('#appview').html('');
		app.Books.forEach(function(x){
			var view = new app.BookItemView({ model: x });
			$('#appview').append( view.render().el );
		});
	}
 
});