/*---------------------------------------------*/
/*                   VIEW                      */
/*---------------------------------------------*/

var app = app || {};

app.BookItemView = Backbone.View.extend({
	tagName: 'li',
	className: 'thumbnail span3',
	template: _.template( $('#t-book-item').html() ),
	events: {
		'click .flag': 'toggleflag'
	},
	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
	},
	render: function() {
		this.$el.html( this.template( this.model.toJSON() ) );
		return this;
	},
	toggleflag: function() {
		
	}
});